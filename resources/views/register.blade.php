<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action = "/welcome" method = "POST">
            @csrf
            <label>First name:</label><br>
            <br>    
            <input type="text" name = "namaDepan"><br>
            <br>
            <label>Last name:</label><br>
            <br>
            <input type="text" name = "namaBelakang"><br>
            <br>
            <label>Gender:</label><br>   
            <br>
            <input type="radio" name ="GenderChoose">Male<br>
            <input type="radio" name ="GenderChoose">Female<br>
            <input type="radio" name ="GenderChoose">Other<br>
            <br>
            <label>Nationality:</label><br>
            <br>
            <select>
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysian</option>
                <option>Australian</option>   
            </select><br>
            <br>
            <label>Language Spoken:</label><br>
            <br>
            <input type = "checkbox">Bahasa Indonesia<br>
            <input type = "checkbox">English<br>
            <input type = "checkbox">Other<br>
            <br>
            <label>Bio:</label><br>
            <br>
            <textarea cols="30" rows = "10"></textarea><br>
            <input type = "submit" value="Sign Up">
        </form>
</body>
</html>